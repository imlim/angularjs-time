﻿var stop;

$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $.ajax({
        url: 'res/remoteBookmarklet.js',
        success: function (data, status, jqXHR) {
            console.log(data);
            $('#bookmarklet').attr('href', data);
        },
        dataType: 'text'
    });
});

function timeDifference(from, to) {
    var fromh = from.split(':')[0];
    var fromm = from.split(':')[1];

    var toh = to.split(':')[0];
    var tom = to.split(':')[1];

    var h = toh - fromh;
    var m = tom - fromm;

    m = m / 60;
    return h + m;
}

function addTime(base, timeToAdd) {
    if (timeToAdd < 0)
        return "Now";
    var split = base.split(':');

    var baseH = parseInt(split[0]);
    var baseM = parseInt(split[1]);

    var newH = baseH + timeToAdd - (timeToAdd % 1);
    var newM = Math.round(baseM + (timeToAdd % 1) * 60);

    while (newM > 59) {
        newM -= 60;
        newH += 1;
    }

    if (newH > 23)
        newH %= 24;
    newH = Math.round(newH);
    newM = Math.round(newM);
    newM = newM.toString().length > 1 ? newM : "0" + newM;

    return newH + ':' + newM;
}

function hoursToTime(hours) {
    var h;
    var m;
    if (hours.toString().indexOf('.') != -1) {
        var split = hours.toString().split('.');
        h = split[0];
        m = Math.round(60 * ("0." + split[1]));
    } else {
        h = hours;
        m = 0;
    }
    if (h.toString().length == 1)
        h = "0" + h;
    if (m.toString().length == 1)
        m = "0" + m;
    return h + ":" + m;
}

function calculate($scope) {
    console.log("Calculating...");
    var length = $scope.times.length;
    var total = 0;

    $scope.resultObjects = [];

    if (length % 2 == 0) {
        // Calculate time
        // Calculate difference between pairs (1, 2); (3, 4); ...
        for (var i = 0; i < length / 2; i++) {
            var start = $scope.times[i * 2];
            var end = $scope.times[i * 2 + 1];

            var diff = timeDifference(start, end);
            total += diff;
        }

        var balance = Math.round((total - $scope.settings.dailyTime) * 100) / 100;
        var time = Math.round(total * 100) / 100;
        $scope.resultObjects.push({title: 'Work time', body: hoursToTime(time) + " / " + time + " h"});
        $scope.resultObjects.push({
            title: 'Time balance',
            body: (balance > 0 ? "+" : "") + hoursToTime(balance) + " / " + (balance > 0 ? "+" : "") + balance + ' h'
        });
    } else { // Predict time

        // Calculate difference between pairs (1, 2); ... - does not include not paired ones
        var i_max = Math.floor(length / 2);
        for (var i = 0; i < i_max; i++) {
            var start = $scope.times[i * 2];
            var end = $scope.times[i * 2 + 1];

            var diff = timeDifference(start, end);
            total += diff;
        }

        var timeLeft = $scope.settings.dailyTime - total;
        var leaveAt = addTime($scope.times[$scope.times.length - 1], timeLeft)

        var roundedTimeLeft = Math.round(timeLeft * 100) / 100;
        $scope.resultObjects.push({
            title: 'Work time left', body: hoursToTime(roundedTimeLeft) + " / " + roundedTimeLeft + ' h'
        });
        $scope.resultObjects.push({title: 'Leave work at', body: leaveAt});
        if (leaveAt != "Now") {
            var dat = new Date();
            var timeFromNow = timeDifference(dat.getHours() + ":" + dat.getMinutes(), leaveAt);
            if (Math.round(timeFromNow * 100) / 100 >= 0)
                $scope.resultObjects.push({
                    title: 'Work time left from now',
                    body: hoursToTime(timeFromNow) + " / " + Math.round(timeFromNow * 100) / 100 + " h"
                });
            else
                $scope.resultObjects.push({
                    title: 'Overtime made til now',
                    body: hoursToTime(timeFromNow * -1) + " / " + Math.round(timeFromNow * -1 * 100) / 100 + " h"
                });
        }
        //refresh();
    }
}

//function refresh ($scope) {
//    stop = $interval(function () {
//        var dat = new Date();
//        var td = timeDifference(dat.getHours() + ":" + dat.getMinutes(), lw);
//        $scope.resultObjects.splice(resultsObjects.length - 1, 1);
//        $scope.resultObjects.push({ title: 'Work time left from now', body: hoursToTime(td) + " / " + Math.round(td * 100) / 100 + " h" });
//    }, 100);
//};