﻿var timeApp = angular.module('timeApp', ['ngAnimate', 'ngCookies']);

timeApp.controller('TimeCtrl', ['$scope', '$document', '$cookies', '$window', function ($scope, $document, $cookies, $window) {
    $scope.version = '1.0.2';
    $scope.times = [];
    $scope.resultObjects = [];

    var updateIt = function () {
        console.log("update!");
        calculate($scope);
        $scope.$apply();
        $window.setTimeout(updateIt, 2000);
    };

    var getSettings = function () {
        var cookieSettings = $cookies.getObject('azra-settings');
        if (cookieSettings == null)
            return {dailyTime: 8.3};
        else
            return cookieSettings;
    };

    var saveSettings = function () {
        $cookies.remove('azra-settings');
        $cookies.putObject('azra-settings', $scope.settings);
    };

    $scope.settings = getSettings();

    $scope.$watch('settings.dailyTime', saveSettings);

    $document.ready(function () {
        var hash = window.location.hash;
        if (hash) {
            var hashStr = hash.substr(1);
            var splitTimes = hashStr.split('/');

            console.log(hashStr, splitTimes);

            for (var i = 0; i < splitTimes.length; i++) {
                console.log("adding... " + splitTimes[i], $scope.addTime(splitTimes[i]));
            }
        }

        $scope.$apply();
        updateIt();
    });

    $scope.deleteTime = function ($index) {
        $scope.times.splice($index, 1);
        calculate($scope);
    };

    $scope.clearAll = function () {
        $scope.times = [];
        calculate($scope);
    };

    $scope.addTimeKeyup = function ($event) {
        var inputgroup = $("#input-add-time-group");
        var regex = /^([01]?\d|2?[0-3]):?[0-5]\d$/;
        var input = $("#input-add-time");
        var value = input.val();

        if ($event.keyCode == 13) /* Enter */ {
            if (regex.test(value)) {
                inputgroup.removeClass("has-error");

                if (value.indexOf(':') == -1)
                    value = value.substring(0, value.length - 2) + ":" + value.substring(value.length - 2);

                if (value.length == 4)
                    value = '0' + value;

                input.val('');

                $scope.addTime(value);
            } else {
                inputgroup.addClass("has-error");
            }
        } else if (inputgroup.hasClass("has-error")) {
            if (regex.test(value))
                inputgroup.removeClass("has-error");
        }
    };

    $scope.addTime = function (time) {
        var regex = /^([01]\d|2[0-3]):[0-5]\d$/;
        if (!regex.test(time))
            return false;

        if ($scope.times.indexOf(time) == -1) {
            $scope.times.push(time);
            $scope.times.sort();

            calculate($scope);
            return true;
        }

        return false;
    };

    $scope.share = function () {
        prompt("Copy this link to share", window.location.protocol + "//" + window.location.hostname + ":" + window.location.port + window.location.pathname + "#" + this.times.join("/"));
    }
}]);